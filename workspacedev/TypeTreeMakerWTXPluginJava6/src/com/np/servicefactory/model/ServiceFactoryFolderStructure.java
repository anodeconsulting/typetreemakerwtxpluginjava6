package com.np.servicefactory.model;

import java.io.Serializable;

public class ServiceFactoryFolderStructure implements Serializable{

	private static final long serialVersionUID = 297344455220587164L;

	private String workingDir;
	
	private String tmpDir;
	private String srcDir;
	
	private String configDir;
	private String dpaDir;
	
	private String infrastructureDir;
	private String applianceDir;
	private String certDir;
	
	private String manitestDir;
	

	
	private String testDir;
	private String testReqDir;
	private String testResDir;
	
	private String xmlDir;
	
	private String wsdlDir;	
	private String xsdDir;
	private String xsltDir;
	
	public String getWorkingDir() {
		return workingDir;
	}
	public void setWorkingDir(String workingDir) {
		this.workingDir = workingDir;
	}
	public String getTmpDir() {
		return tmpDir;
	}
	public void setTmpDir(String tmpDir) {
		this.tmpDir = tmpDir;
	}
	public String getSrcDir() {
		return srcDir;
	}
	public void setSrcDir(String srcDir) {
		this.srcDir = srcDir;
	}
	public String getConfigDir() {
		return configDir;
	}
	public void setConfigDir(String configDir) {
		this.configDir = configDir;
	}
	public String getDpaDir() {
		return dpaDir;
	}
	public void setDpaDir(String dpaDir) {
		this.dpaDir = dpaDir;
	}
	public String getInfrastructureDir() {
		return infrastructureDir;
	}
	public void setInfrastructureDir(String infrastructureDir) {
		this.infrastructureDir = infrastructureDir;
	}
	public String getApplianceDir() {
		return applianceDir;
	}
	public void setApplianceDir(String applianceDir) {
		this.applianceDir = applianceDir;
	}
	public String getCertDir() {
		return certDir;
	}
	public void setCertDir(String certDir) {
		this.certDir = certDir;
	}
	public String getManitestDir() {
		return manitestDir;
	}
	public void setManitestDir(String manitestDir) {
		this.manitestDir = manitestDir;
	}
	public String getTestDir() {
		return testDir;
	}
	public void setTestDir(String testDir) {
		this.testDir = testDir;
	}
	public String getTestReqDir() {
		return testReqDir;
	}
	public void setTestReqDir(String testReqDir) {
		this.testReqDir = testReqDir;
	}
	public String getTestResDir() {
		return testResDir;
	}
	public void setTestResDir(String testResDir) {
		this.testResDir = testResDir;
	}
	public String getXmlDir() {
		return xmlDir;
	}
	public void setXmlDir(String xmlDir) {
		this.xmlDir = xmlDir;
	}
	public String getWsdlDir() {
		return wsdlDir;
	}
	public void setWsdlDir(String wsdlDir) {
		this.wsdlDir = wsdlDir;
	}
	public String getXsdDir() {
		return xsdDir;
	}
	public void setXsdDir(String xsdDir) {
		this.xsdDir = xsdDir;
	}
	public String getXsltDir() {
		return xsltDir;
	}
	public void setXsltDir(String xsltDir) {
		this.xsltDir = xsltDir;
	}
}
