/**
 * 
 */
package com.np.servicefactory.model;

import java.util.ArrayList;

/**
 * @author Wang
 *
 */
public class ResponseConfigService {
	public String serviceName;	
	public String serviceTargetNameSpace;
	
	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getServiceTargetNameSpace() {
		return serviceTargetNameSpace;
	}

	public void setServiceTargetNameSpace(String serviceTargetNameSpace) {
		this.serviceTargetNameSpace = serviceTargetNameSpace;
	}

	public ArrayList<OperationDoc> operationDoc;
	
	/**
	 * 
	 */
	public ResponseConfigService() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public ArrayList<OperationDoc> getOperationDoc() {
		return operationDoc;
	}

	public void setOperationDoc(ArrayList<OperationDoc> operationDoc) {
		this.operationDoc = operationDoc;
	}

}
